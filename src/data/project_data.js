export const dobda = {
  images: ['/images/dobda/main.jpg'],
  name: 'DOBDA',
  subName: '지역 주민 간의 의뢰 사이트',
  period: '2022/02/18 ~ 2022/03/21',
  linkText: 'DOBDA 시연 사이트로 이동',
  link: 'https://jhlee-ac68u.asuscomm.com:25143',
  detailLinkText: '상세정보 보러 가기',
  detailLink:
    'https://gitlab.com/kakaruu/resume/-/blob/master/Projects.md#%EC%9D%98%EB%A2%B0-%EA%B3%B5%EC%9C%A0-%EC%82%AC%EC%9D%B4%ED%8A%B8-dobda',
  description: `멀티캠퍼스의 풀스택 개발 과정을 수행하며, 개인 팀 프로젝트에서 만든 이웃 간 의뢰 사이트입니다.
회원 간의 사소한 문제거리들을 의뢰하고 대금을 전달할 수 있는 사이트를 평소에 만들어보고 싶었던 터라 아이디어를 제안하고 만들게 되었습니다.
의뢰 글을 작성하는 게시판을 중심으로, 사용자 간의 채팅과 결제, 거래 평가, 알림 기능들이 구성되어 있습니다.
저는 팀장으로서 4명의 팀원분들과 함께 이 프로젝트를 이끌어가게 되었습니다.

제가 이 프로젝트에서 사용한 주요 기술들은 아래와 같습니다.
- 프론트
  - React 17
  - React-redux
  - 오픈뱅킹 API
  - 카카오 지도 API
- 백엔드
  - Spring boot 2.6.3
  - Spring Data JPA
  - Spring security
  - Spring security message + Websocket STOMP
  - 오픈뱅킹 API
- DevOps
  - Docket
  - GitLab CI/CD
  - Naver Cloud Platform
  - MySQL
- 형상관리
  - Git
  - GitLab`,
};

export const pickmeup = {
  images: ['/images/pickmeup/main.jpg'],
  name: '픽미업',
  subName: '지역 상권 쇼핑몰 사이트',
  period: '2022/01/21 ~ 2022/02/14',
  linkText: '픽미업 시연 사이트로 이동',
  link: 'https://jhlee-ac68u.asuscomm.com:25280',
  detailLinkText: '상세정보 보러 가기',
  detailLink:
    'https://gitlab.com/kakaruu/resume/-/blob/master/Projects.md#%EC%A7%80%EC%97%AD-%EC%83%81%EA%B6%8C-%EC%87%BC%ED%95%91%EB%AA%B0-%ED%94%BD%EB%AF%B8%EC%97%85',
  description: `멀티캠퍼스의 풀스택 개발 과정을 수행하며, 개인 팀 프로젝트에서 만들었던 쇼핑몰 사이트입니다.
저의 주변 주역에 있는 작은 상인분들이 상품을 판매할 수 있는 사이트가 있으면 좋겠다는 생각으로 만들어보게 되었습니다.
판매자 회원이 등록하는 상품정보를 중심으로 장바구니, 구매, 결제, 후기 기능들이 구성되어 있습니다.
저는 팀장으로서 4명의 팀원분들과 함께 이 프로젝트를 이끌어가게 되었습니다.
처음으로 팀장이라는 위치에서 프로젝트를 이끌어가면서, 모두가 만족할 수 있는 환경을 만드는 것이 얼마나 어려운 것인지 이러한 과정을 거치면서, 팀장분들의 고충들을 조금이나마 공감할 수 있게 되지 않았나 싶습니다.

제가 이 프로젝트에서 사용한 주요 기술들은 아래와 같습니다.
- 프론트
  - React 17
  - React-redux
- 백엔드
  - Spring boot 2.6.3
  - Spring Data JPA
  - Spring security
- DevOps
  - Docker
  - GitLab CI/CD
  - Naver Cloud Platform
  - MySQL
- 형상관리
  - Git
  - GitLab`,
};
