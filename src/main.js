import Vue from 'vue';
import App from './App.vue';

// FontAwesome 아이콘 세트 설정
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
Vue.component('fa-icon', FontAwesomeIcon);

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowAltCircleDown,
  faRocket,
  faPortrait,
  faEnvelope,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import { faGitlab, faFacebookF } from '@fortawesome/free-brands-svg-icons';
library.add(
  faArrowAltCircleDown,
  faRocket,
  faPortrait,
  faEnvelope,
  faGitlab,
  faFacebookF,
  faTimes
);

// bootstrap-vue
import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

// vue-scrollto
import VueScrollTo from 'vue-scrollto';
Vue.use(VueScrollTo, {
  offset: -62,
  duration: 0,
});

new Vue({
  el: '#app',
  render: (h) => h(App),
});
